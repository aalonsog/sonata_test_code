var XMLHttpRequest = require("./xmlhttprequest").XMLHttpRequest;

exports.sendData = function(options, data, callBackOK, callbackError) {
    var xhr, body, result;

    var url = "http://" + options.host + ":" + options.port + options.path;
    xhr = new XMLHttpRequest();
    xhr.open(options.method, url, true);
    if (options.headers["content-type"]) {
        xhr.setRequestHeader("Content-Type", options.headers["content-type"]);
    }
    for (var headerIdx in options.headers) {
        xhr.setRequestHeader(headerIdx, options.headers[headerIdx]);
    }

    xhr.onerror = function(error) {
    }
    xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {
            if (xhr.status < 400) {
                callBackOK(xhr.status, xhr.responseText, xhr.getAllResponseHeadersList());
            } else {
                callbackError(xhr.status, xhr.responseText);
            }
        }
    };

    if (data !== undefined) {
        try {
            xhr.send(data);
        } catch (e) {
            callbackError(e.message);
            return;
        }
    } else {
        try {
            xhr.send();
        } catch (e) {
            callbackError(e.message);
            return;
        }
    }
}