var http = require('./httpClient');
var should = require('should');
var mocha = require('mocha'); 

describe('Unit tests', function() {

	before(function(done) {
		var express = require('express');
		var app = express();

		var db_connection = require('./../db/connection');
		var auth = require('./../routes/auth');
		var user = require('./../routes/user');
		var todo = require('./../routes/todo');

		app.set('port', process.env.PORT || 8080);
		app.use(express.logger('dev'));
		app.use(express.json());
		app.use(express.urlencoded());
		app.use(express.methodOverride());
		app.use(express.cookieParser('your secret here'));
		app.use(express.session());
		app.use(app.router);

		// Development only
		if ('development' == app.get('env')) {
		  app.use(express.errorHandler());
		}

		// Request handlers
		app.all('/todo/:id', auth.check_auth);

		app.post('/todo/', todo.create);
		app.put('/todo/:id', todo.update);
		app.delete('/todo/:id', todo.delete);

		app.get('/user/:id', user.list);

		db_connection.connect(function() {
			app.listen(app.get('port'), function(){
			  console.log('TODO server listening on port ' + app.get('port'));
			  done();
			});
		});
    });

	var todoId;
	var sessionId = "123456";
	var userId = "12";

	it('should create a todo', function (done) {

		var options = {
        host: 'localhost',
        port: 8080,
        path: '/todo/',
        method: 'POST',
        headers: {"Content-Type": "application/json", "UserId": userId, "SessionId": sessionId}
	    };
	    var body = {title: 'todo1', text: 'My first todo'};
	    http.sendData(options, JSON.stringify(body), function(status, resp) {

	    	if (status === 200) {

	    		console.log(resp)
	    		todoId = resp;
	    		done();
	    	} else {
	    		throw resp;
	    	}

	    }, function(e){
	    	throw e;
	    });
	  
	});

	it('should get the todo', function (done) {

		var options = {
        host: 'localhost',
        port: 8080,
        path: '/user/' + userId,
        method: 'GET',
        headers: {"Content-Type": "application/json", "UserId": userId, "SessionId": sessionId}
	    };
	    http.sendData(options, undefined, function(status, resp) {

	    	if (status === 200) {
	    		var todos = JSON.parse(resp);
	    		for(var t in todos) {
	    			if (todos[t]._id == todoId) {
	    				done();
	    			}
	    		}	    	
	    	} else {
	    		throw resp;
	    	}

	    }, function(e){
	    	throw e
	    });
	  
	});

	it('should update the todo', function (done) {

		var options = {
        host: 'localhost',
        port: 8080,
        path: '/todo/' + todoId,
        method: 'PUT',
        headers: {"Content-Type": "application/json", "UserId": userId, "SessionId": sessionId}
	    };
	    var body = {title: 'todo1_modified'};
	    http.sendData(options, JSON.stringify(body), function(status, resp) {

	    	if (status === 200) {
	    		done();
	    			    	
	    	} else {
	    		throw resp;
	    	}

	    }, function(e){
	    	throw e
	    });
	});

	it('should get the updated todo', function (done) {

		var options = {
        host: 'localhost',
        port: 8080,
        path: '/user/' + userId,
        method: 'GET',
        headers: {"Content-Type": "application/json", "UserId": userId, "SessionId": sessionId}
	    };
	    http.sendData(options, undefined, function(status, resp) {

	    	if (status === 200) {
	    		var todos = JSON.parse(resp);
	    		for(var t in todos) {
	    			if (todos[t]._id == todoId) {
	    				if(todos[t].title === 'todo1_modified') {	
	    					done();
	    				}
	    			}
	    		}	    	
	    	} else {
	    		throw resp;
	    	}

	    }, function(e){
	    	throw e
	    });
	});

	it('should not update a todo that a user does not own', function (done) {

		var options = {
        host: 'localhost',
        port: 8080,
        path: '/todo/' + todoId,
        method: 'PUT',
        headers: {"Content-Type": "application/json", "UserId": "3333", "SessionId": sessionId}
	    };
	    var body = {title: 'todo1_modified'};
	    http.sendData(options, JSON.stringify(body), function(status, resp) {
	    }, function(e){
	    	if (e === 401) {
	    		done();
	    	}
	    });
	});

	it('should not delete a todo that a user does not own', function (done) {

		var options = {
        host: 'localhost',
        port: 8080,
        path: '/todo/' + todoId,
        method: 'DELETE',
        headers: {"UserId": "33333", "SessionId": sessionId}
	    };
	    http.sendData(options, undefined, function(status, resp) {
	
	    }, function(e){
	    	if (e === 401) {
	    		done();
	    	}
	    });
	});
    
    it('should delete a todo', function (done) {

		var options = {
        host: 'localhost',
        port: 8080,
        path: '/todo/' + todoId,
        method: 'DELETE',
        headers: {"UserId": userId, "SessionId": sessionId}
	    };
	    http.sendData(options, undefined, function(status, resp) {
	    	if (status === 200) {
	    		done();
	    	}
	
	    }, function(e){
	    	throw e
	    });
	});

});

