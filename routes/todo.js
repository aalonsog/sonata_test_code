var todo_db = require('./../db/todo_db');

// Creates a todo for a user
exports.create = function(req, res){

	var todo = check_body(req.body);
	var userid = req.headers['userid'];

	console.log('Creating todo ', todo, 'for user ', userid);

	todo_db.create(userid, todo, function(new_todo) {
		res.send(new_todo.id);
	}, function(e) {
		res.send(500);
	});
};

// Modifies a todo of a user
exports.update = function(req, res){

	var todoId = req.params.id;
	var todo = check_body(req.body);

	console.log('Updating todo', todoId);

	todo_db.update(todoId, todo, function() {
		res.send();
	}, function(e) {
		res.send(500);
	});
};

// Deletes a todo from a user
exports.delete = function(req, res){

	var todoId = req.params.id;

	console.log('Deleting todo', todoId);
	todo_db.delete(todoId, function() {
		res.send();
	}, function(e) {
		res.send(500);
	});
};

// Checks if the body of create and update requests has the correct format
var check_body = function (body) {
	var todo = {};

	if (body.title) {
		todo.title = body.title;
	}
	if (body.text) {
		todo.text = body.text;
	}

	return todo;
};