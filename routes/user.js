var todo_db = require('./../db/todo_db');

// Lists the todos of a determined user
exports.list = function(req, res){

	var userId = req.params.id;

	console.log('Listing todos for user ', userId);

	todo_db.get_all(userId, function(todos) {
		res.send(todos);
	}, function(e) {
		res.send(500);
	});

};