var todo_db = require('./../db/todo_db');

// This value indicates the probability (between 0 and 1) of successfully pass the mocked session authorization
var FAKE_AUTH_SUCCESS_PROBABILITY = 0.9;

// Checks if the request is correcty authorized
exports.check_auth = function(req, res, next){

	var userId = req.headers['userid'];
	var sessionId = req.headers['sessionid'];
	console.log('Checking auth for user ', userId);

	// First check if credentials exist
	if (!userId || !sessionId) {
	  	res.send(401, 'Authentication needed');
	  	return;
	}

	// Second check if the session is valid
	check_session(sessionId, function() {

		// Finally check if user is authorized to access the resource. 

		var todoId = req.params.id;

		todo_db.get(todoId, function(todo) {

			if (todo === null) {
				res.send(404);
				return;
			}
			if (todo.userId == userId) {
				next();
			} else {
				res.send(401, 'Unauthorized');
	  			return;
			}

		}, function(e) {
			res.send(404, 'Resource does not exist');
			return;
		});

	}, function(e) {
		console.log('Mocked session auth failed');
	  	res.send(401, 'Unauthorized');
	  	return;
	});
};

// Checks if the session data of a request is valid
var check_session = function(sessionId, callback, callback_error) {

	var rand = Math.random();
	if (rand < FAKE_AUTH_SUCCESS_PROBABILITY) {
		callback();
	} else {
		callback_error();
	}
};