var express = require('express');
var app = express();

var db_connection = require('./db/connection');
var auth = require('./routes/auth');
var user = require('./routes/user');
var todo = require('./routes/todo');

app.set('port', process.env.PORT || 8080);
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);

// Development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// Request handlers
app.all('/todo/:id', auth.check_auth);

app.post('/todo/', todo.create);
app.put('/todo/:id', todo.update);
app.delete('/todo/:id', todo.delete);

app.get('/user/:id', user.list);

db_connection.connect(function() {
	app.listen(app.get('port'), function(){
	  console.log('TODO server listening on port ' + app.get('port'));
	});
});
