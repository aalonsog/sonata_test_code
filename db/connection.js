var mongoose = require('mongoose');

exports.connect = function(callback) {

	mongoose.connect('mongodb://localhost/sonata_test_code');
	var db = mongoose.connection;

	db.on('error', console.error.bind(console, 'connection error:'));

	db.once('open', function() {

	  	console.log('Successfully connected to mongo database');
	  	callback();
	});
};

