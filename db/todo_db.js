var mongoose = require('mongoose');

// Todo data base schema
var todoSchema = mongoose.Schema({
	userId: Number,
    title: String,
    text: String
});

// Todo data base model
var Todo = mongoose.model('Todo', todoSchema);

	exports.get_all = function (userId, callback, callback_error) {

		Todo.find({userId: userId}, function (e, todos) {
	  	if (e) {
	  		callback_error(e);
	  	} else {
	  		callback(todos);
	  	}
	});
};

// Todo data base methods
exports.get = function (todoId, callback, callback_error) {

	Todo.findById(todoId, function (e, todo) {
	  	if (e) {
	  		callback_error(e);
	  	} else {
	  		callback(todo);
	  	}
	});
};

exports.create = function (userId, todo, callback, callback_error) {

	var t = new Todo({userId: userId, title: todo.title, text: todo.text});

	t.save(function (e, new_todo) {
	  	if (e) {
	  		callback_error(e);
	  	} else {
	  		callback(new_todo);
	  	}
	});
};

exports.update = function (todoId, todo, callback, callback_error) {

	var id = mongoose.Types.ObjectId(todoId);

	Todo.update({_id: id}, todo, function (e) {
	  	if (e) {
	  		callback_error(e);
	  	} else {
	  		callback();
	  	}
	});
};

exports.delete = function (todoId, callback, callback_error) {

	var id = mongoose.Types.ObjectId(todoId);

	Todo.remove({_id: id}, function (e) {
	  	if (e) {
	  		callback_error(e);
	  	} else {
	  		callback();
	  	}
	});
};